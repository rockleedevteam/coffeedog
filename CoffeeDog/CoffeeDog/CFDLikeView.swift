//
//  CFDLikeView.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2015-12-22.
//  Copyright © 2015 Rock Lee. All rights reserved.
//

import UIKit

class CFDLikeView: UIView {
    let likeButton = UIButton()
    let likeLabel = UILabel()
    
    var liked: Bool = false {
        didSet {
            if liked {
                likeButton.setImage(UIImage(named: "redLike"), for: UIControlState())
            } else {
                likeButton.setImage(UIImage(named: "like"), for: UIControlState())
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let numberString = NSAttributedString(string: "20人")
        let likeString = NSAttributedString(string: " like", attributes: [NSForegroundColorAttributeName:UIColor.red])
        let attributedString = NSMutableAttributedString(attributedString: numberString)
        attributedString.append(likeString)
        likeLabel.attributedText = attributedString
        likeLabel.textAlignment = .center
        likeLabel.font = UIFont.systemFont(ofSize: 8)
        
        likeButton.setImage(UIImage(named: "like"), for: UIControlState())
        likeButton.addTarget(self, action: #selector(CFDLikeView.likeAction), for: UIControlEvents.touchUpInside)
        
        likeLabel.translatesAutoresizingMaskIntoConstraints = false
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(likeLabel)
        addSubview(likeButton)
        
        let _ = pinItemFillHorizontally(likeButton)
        let _ = pinItemFillHorizontally(likeLabel)
        NSLayoutConstraint(item: likeButton, attribute: .centerX, relatedBy: .equal, toItem: likeLabel, attribute:.centerX, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: likeButton, attribute: .bottom, relatedBy: .equal, toItem: likeLabel, attribute:.top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: likeButton, attribute: .top, relatedBy: .equal, toItem: self, attribute:.top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: likeLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute:.bottom, multiplier: 1.0, constant: 0).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func likeAction() {
        liked = !liked
    }
}
