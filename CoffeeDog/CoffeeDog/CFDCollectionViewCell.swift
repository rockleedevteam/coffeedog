//
//  CFDCollectionViewCell.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-22.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

class CFDCollectionViewCell: UICollectionViewCell {
    
    class func cellReuseIdentifier() -> String {
        return "cvc_\(NSStringFromClass(self))"
    }
    
    class func registerClass(_ collectionView: UICollectionView) {
        collectionView.register(self, forCellWithReuseIdentifier: self.cellReuseIdentifier())
    }
    
}
