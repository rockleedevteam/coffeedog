//
//  CFDCollectionHeaderView.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-22.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

class CFDTextHeaderView: CFDCollectionReusableView {
    
    class func dequeueReusableHeader(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> CFDTextHeaderView {
        return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: self.viewReuseIdentifier(), for: indexPath) as! CFDTextHeaderView
    }
    
    class func defaultHeight() -> CGFloat {
        return 55
    }
    
    let mainLabel = CFDLabel()
    let secondaryLabel = CFDLabel()
    
    override func initialSetup() {
        super.initialSetup()
        
        for label in [mainLabel, secondaryLabel] {
            label.font = UIFont.systemFont(ofSize: 12)
            label.textAlignment = .center
            contentView.addSubview(label)
        }
        
        setupViewConstraints()
    }
    
    func setupViewConstraints() {
        let constraints = ["H:|-[mainLabel]-|",
                           "H:|-[secondaryLabel]-|",
                           "V:|-[mainLabel][secondaryLabel]-|"]
        let bindings = ["mainLabel" : mainLabel,
                        "secondaryLabel" : secondaryLabel]
        let _ = contentView.addVisualConstraints(constraints, bindings: bindings)
        
        let _ = contentView.pinItemCenterHorizontally(mainLabel)
        let _ = contentView.pinItemCenterHorizontally(secondaryLabel)
    }
    
}
