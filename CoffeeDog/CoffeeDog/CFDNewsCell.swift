//
//  CFDCollectionViewCell.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2015-12-16.
//  Copyright © 2015 Rock Lee. All rights reserved.
//

import UIKit

class CFDNewsCell: CFDCollectionViewCell {
    
    class func dequeueReusableCell(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> CFDNewsCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: self.cellReuseIdentifier(), for: indexPath) as! CFDNewsCell
    }
    
    let typeLabel = UILabel()
    let mediaView = UIView()
    let imageView = UIImageView()
    let titleLabel = UILabel()
    let subTitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        for view in [typeLabel, mediaView, titleLabel, subTitleLabel] {
            view.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(view)
        }
        
        let views = [
            "typeLabel" : typeLabel,
            "mediaView" : mediaView,
            "titleLabel" : titleLabel,
            "subTitleLabel" : subTitleLabel,
                    ]
        let formats = [
            "V:|-[typeLabel]-[mediaView]-[titleLabel]-[subTitleLabel]-|",
            "H:|-[typeLabel]-|",
            "H:|[mediaView]|",
            "H:|-[titleLabel]-|",
            "H:|-[subTitleLabel]-|",
                        ]
        
        for format in formats {
            let constraint = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
            NSLayoutConstraint.activate(constraint)
        }
        
        typeLabel.font = UIFont.systemFont(ofSize: 12)
        typeLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: UILayoutConstraintAxis.vertical)
        
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        subTitleLabel.textAlignment = .center
        subTitleLabel.font = UIFont.systemFont(ofSize: 10)

        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        mediaView.addSubview(imageView)
        let _ = mediaView.pinItemFillAll(imageView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
