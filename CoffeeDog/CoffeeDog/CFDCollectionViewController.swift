//
//  CFDCollectionViewController.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2015-12-19.
//  Copyright © 2015 Rock Lee. All rights reserved.
//

import UIKit

class CFDCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    init() {
        let layout = UICollectionViewFlowLayout()
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.white
        
        let titleImage = UIImage(named: "Title")
        let titleView = UIImageView(image: titleImage)
        navigationItem.titleView = titleView
        
        let searchButton = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchAction(_:)))
        searchButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = searchButton
    }
    
    // MARK: - Actions
    
    func searchAction(_ sender: AnyObject) {
        
    }
}
