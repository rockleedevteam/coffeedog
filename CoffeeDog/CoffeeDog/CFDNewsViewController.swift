//
//  CFDNewsViewController.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2015-12-15.
//  Copyright © 2015 Rock Lee. All rights reserved.
//

import UIKit

class CFDNewsViewController: CFDCollectionViewController {
    
    override init() {
        super.init()
        title = "新闻"
        tabBarItem.image = UIImage(named: "News")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CFDTextHeaderView.registerHeaderClass(collectionView!)
        CFDNewsCell.registerClass(collectionView!)
    }
    
    // MARK: - UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CFDNewsCell.dequeueReusableCell(collectionView, forIndexPath: indexPath)
        
        switch indexPath.item {
        case 0, 3:
            cell.typeLabel.text = "/ 文章"
            cell.titleLabel.text = "零基础咖啡指南，咖啡菜鸟必看！"
            cell.subTitleLabel.text = "本文为大家介绍八种常见的咖啡，五分钟可以完成扫盲。"
            cell.imageView.image = UIImage(named: "coffee_label_dribbble")
        case 1, 4:
            cell.typeLabel.text = "/ 视频"
            cell.titleLabel.text = "零基础咖啡指南，咖啡菜鸟必看！"
            cell.subTitleLabel.text = "本文为大家介绍八种常见的咖啡，五分钟可以完成扫盲。"
            cell.imageView.image = UIImage(named: "D4Y_105U")
        case 2, 5:
            cell.typeLabel.text = "/ 视频"
            cell.titleLabel.text = "零基础咖啡指南，咖啡菜鸟必看！"
            cell.subTitleLabel.text = "本文为大家介绍八种常见的咖啡，五分钟可以完成扫盲。"
            cell.imageView.image = UIImage(named: "desk")
        default:
            break
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = CFDTextHeaderView.dequeueReusableHeader(collectionView, forIndexPath: indexPath)
        header.mainLabel.text = "today's news"
        header.secondaryLabel.text = "每日新闻"
        return header
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.bounds.width, height: CFDTextHeaderView.defaultHeight())
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let goldenRatio: CGFloat = 1.61803398875
        let height = view.bounds.width / goldenRatio
        return CGSize(width: view.bounds.width, height: height)
    }
}
