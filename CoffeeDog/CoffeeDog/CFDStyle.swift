//
//  CFDStyle.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-21.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

class CFDStyle {
    
    class func colorGreen() -> UIColor {
        return UIColor(red: 82/255.0, green: 158/255.0, blue: 122/255.0, alpha: 1.0)
    }
    
    class func colorDarkGreen() -> UIColor {
        return UIColor(red: 63/255.0, green: 159/255.0, blue: 117/255.0, alpha: 1.0)
    }
    
    class func colorGrey() -> UIColor {
        return UIColor(red: 216/255.0, green: 215/255.0, blue: 216/255.0, alpha: 0.95)
    }
    
    class func colorDarkGrey() -> UIColor {
        return UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 0.95)
    }
    
}
