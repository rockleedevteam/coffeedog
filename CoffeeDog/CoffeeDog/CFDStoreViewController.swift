//
//  CFDStoreViewController.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2015-12-22.
//  Copyright © 2015 Rock Lee. All rights reserved.
//

import UIKit

class CFDStoreViewController: CFDCollectionViewController {
    
    override init() {
        super.init()
        title = "商店"
        tabBarItem.image = UIImage(named: "Shop")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}
