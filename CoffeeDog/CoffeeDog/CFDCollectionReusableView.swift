//
//  CFDCollectionReusableView.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-22.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

class CFDCollectionReusableView: UICollectionReusableView {
    
    let contentView = CFDView()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var initialSetupDone = false
    
    func initialSetup() {
        assert(initialSetupDone == false)
        
        addSubview(contentView)
        let _ = pinItemFillAll(contentView)
        
        initialSetupDone = true
    }
    
    class func viewReuseIdentifier() -> String {
        return "cvrv_\(NSStringFromClass(self))"
    }
    
    class func registerHeaderClass(_ collectionView: UICollectionView) {
        collectionView.register(self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: self.viewReuseIdentifier())
    }
    
    class func registerFooterClass(_ collectionView: UICollectionView) {
        collectionView.register(self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: self.viewReuseIdentifier())
    }
    
    
}
