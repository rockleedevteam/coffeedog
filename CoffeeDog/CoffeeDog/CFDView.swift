//
//  CFDView.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-22.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

class CFDView: UIView {
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
