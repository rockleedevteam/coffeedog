//
//  UIView+Extensions.swift
//  CoffeeDog
//
//  Created by Rock Lee on 2016-05-22.
//  Copyright © 2016 Rock Lee. All rights reserved.
//

import UIKit

extension UIView {
    
    func addSubviews(_ views: [UIView]) {
        for view in views {
            addSubview(view)
        }
    }
    
    // MARK: - Auto Layout
    
    func pinItem(_ item: AnyObject, attribute: NSLayoutAttribute, to toItem: AnyObject, toAttribute: NSLayoutAttribute?=nil, withOffset offset: CGFloat=0, andScale scale: CGFloat=1, priority: UILayoutPriority?=nil) -> NSLayoutConstraint {
        
        let constraint = NSLayoutConstraint(item: item, attribute: attribute, relatedBy: .equal, toItem: toItem, attribute: toAttribute ?? attribute, multiplier: scale, constant: offset)
        
        if let priority = priority {
            constraint.priority = priority
        }
        
        self.addConstraint(constraint)
        return constraint
    }
    
    func addVisualConstraints(_ constraints: [String], bindings:[String : AnyObject], metrics:[String : String]?=nil) -> [NSLayoutConstraint] {
        
        var resultConstraints = [NSLayoutConstraint]()
        
        for constraint in constraints {
            resultConstraints += NSLayoutConstraint.constraints(withVisualFormat: constraint, options: NSLayoutFormatOptions.init(rawValue: 0), metrics: metrics, views: bindings)
        }
        
        addConstraints(resultConstraints)
        
        return resultConstraints
    }
    
    // MARK: - Convenience
    
    // Center
    
    func pinItemCenterHorizontally(_ item: AnyObject, to toItem: AnyObject?=nil, withOffset offset: CGFloat=0) -> NSLayoutConstraint {
        if let toItem = toItem {
            return pinItem(item, attribute: .centerX, to: toItem, withOffset: offset)
        } else {
            return pinItem(item, attribute: .centerX, to: self, withOffset: offset)
        }
    }

    func pinItemCenterVertically(_ item: AnyObject, to toItem: AnyObject?=nil, withOffset offset: CGFloat=0) -> NSLayoutConstraint {
        if let toItem = toItem {
           return pinItem(item, attribute: .centerY, to: toItem, withOffset: offset)
        } else {
            return pinItem(item, attribute: .centerY, to: self, withOffset: offset)
        }
    }
    
    // Fill
    
    func pinItemFillHorizontally(_ item: AnyObject) -> [NSLayoutConstraint] {
        return [pinItem(item, attribute: .left, to: self), pinItem(item, attribute: .right, to: self)]
    }
    
    func pinItemFillVertically(_ item: AnyObject) -> [NSLayoutConstraint] {
        return [pinItem(item, attribute: .top, to: self), pinItem(item, attribute: .bottom, to: self)]
    }
    
    func pinItemFillMarginsHorizontally(_ item: AnyObject) -> [NSLayoutConstraint] {
        return [pinItem(item, attribute: .left, to: self, toAttribute: .leftMargin), pinItem(item, attribute: .right, to: self, toAttribute: .rightMargin)]
    }
    
    func pinItemFillMarginsVertically(_ item: AnyObject) -> [NSLayoutConstraint] {
        return [pinItem(item, attribute: .top, to: self, toAttribute: .topMargin), pinItem(item, attribute: .bottom, to: self, toAttribute: .bottomMargin)]
    }
    
    func pinItemFillAll(_ item: AnyObject) -> [NSLayoutConstraint] {
        return pinItemFillHorizontally(item) + pinItemFillVertically(item)
    }
    
    func pinItemFillMarginsAll(_ item: AnyObject) -> [NSLayoutConstraint] {
        return pinItemFillMarginsHorizontally(item) + pinItemFillMarginsVertically(item)
    }
    
    // Others
    
    func pinItemPosition(_ item: AnyObject, to toItem: AnyObject?) -> [NSLayoutConstraint] {
        return [pinItemCenterHorizontally(item, to: toItem), pinItemCenterVertically(item, to: toItem)]
    }
    
    func pinItemSize(_ item: AnyObject, to toItem: AnyObject?) -> [NSLayoutConstraint] {
        if let toItem = toItem {
            return [pinItem(item, attribute: .height, to: toItem), pinItem(item, attribute: .width, to: toItem)]
        } else {
            return [pinItem(item, attribute: .height, to: self), pinItem(item, attribute: .width, to: self)]
        }
    }
    
    func pinItemSquare() -> NSLayoutConstraint {
        return superview!.pinItem(self, attribute: .height, to: self, toAttribute: .width)
    }
}
